'use strict';

(function () {
    const navbar = $('#navbar'),
        sticky = navbar.offset().top;

    if ($('body').attr('id') === 'example-1') {
        stickyHeader();
    } else {
        translateHeader();
    }

    function stickyHeader() {
        window.onscroll = function () {
            setStickyHeader();
        };

        function setStickyHeader() {
            if (window.pageYOffset >= sticky) {
                navbar.addClass('sticky');
            } else {
                navbar.removeClass('sticky');
            }
        }
    }

    function translateHeader() {
        let lastScrollTop = 0;
        window.onscroll = function () {
            setStickyHeader();

            let scrollTop = $(this).scrollTop();
            if (scrollTop > lastScrollTop) {
                // Down
                if (!navbar.hasClass('hide')) {
                    navbar.addClass('hide');
                }
            } else {
                // Up
                if (navbar.hasClass('hide')) {
                    navbar.removeClass('hide');
                }
            }
            lastScrollTop = scrollTop;
        };

        function setStickyHeader() {
            if (window.pageYOffset >= sticky) {
                navbar.addClass('sticky hide');
            } else {
                navbar.removeClass('sticky hide');
            }
        }
    }
})();

